module data_configure
(
	input wire  [4:0] 	cnt_bit,//哪一bit
	input wire  [6:0] 	cnt_led,//64个灯中哪个灯
	input wire  [2:0]	cnt_photo,//哪副图

	output 	reg   		bit
);
	

wire 	[23:0] data[63:0];
wire 	[23:0] data1[63:0];
wire 	[23:0] data2[63:0];

always @ (*) begin
	case (cnt_photo)
		3'b000:bit = data[cnt_led][23 - cnt_bit];
		3'b001:bit = data1[cnt_led][23 - cnt_bit];
		3'b010:bit = data2[cnt_led][23 - cnt_bit];
		3'b011:bit = data1[cnt_led][23 - cnt_bit];
		3'b100:bit = data[cnt_led][23 - cnt_bit];
		default: ;
	endcase
end
//GRB
//先列选
//ctrl alt i
//1
assign data[00] = {8'd00,8'd00,8'd00};
assign data[01] = {8'd00,8'd00,8'd00};
assign data[02] = {8'd00,8'd00,8'd00};
assign data[03] = {8'd00,8'd00,8'd00};
assign data[04] = {8'd00,8'd00,8'd00};
assign data[05] = {8'd00,8'd00,8'd00};
assign data[06] = {8'd00,8'd00,8'd00};
assign data[07] = {8'd00,8'd00,8'd00};
//2

assign data[08] = {8'd00,8'd00,8'd00};
assign data[09] = {8'd00,8'd00,8'd00};
assign data[10] = {8'd00,8'd00,8'd00};
assign data[11] = {8'd00,8'd00,8'd00};
assign data[12] = {8'd00,8'd00,8'd00};
assign data[13] = {8'd00,8'd00,8'd00};
assign data[14] = {8'd00,8'd00,8'd00};
assign data[15] = {8'd00,8'd00,8'd00};
//3
assign data[16] = {8'd00,8'd00,8'd00};
assign data[17] = {8'd00,8'd00,8'd00};
assign data[18] = {8'd00,8'd00,8'd00};
assign data[19] = {8'd00,8'd00,8'd00};
assign data[20] = {8'd00,8'd00,8'd00};
assign data[21] = {8'd00,8'd00,8'd00};
assign data[22] = {8'd00,8'd00,8'd00};
assign data[23] = {8'd00,8'd00,8'd00};
//4
assign data[24] = {8'd00,8'd00,8'd00};
assign data[25] = {8'd00,8'd00,8'd00};
assign data[26] = {8'd00,8'd00,8'd00};
assign data[27] = {8'd00,8'd00,8'd00};
assign data[28] = {8'd00,8'd00,8'd00};
assign data[29] = {8'd00,8'd00,8'd00};
assign data[30] = {8'd00,8'd00,8'd00};
assign data[31] = {8'd00,8'd00,8'd00};
//5
assign data[32] = {8'd00,8'd00,8'd00};
assign data[33] = {8'd00,8'd00,8'd00};
assign data[34] = {8'd00,8'd00,8'd00};
assign data[35] = {8'd00,8'd00,8'd00};
assign data[36] = {8'd00,8'd00,8'd00};
assign data[37] = {8'd00,8'd00,8'd00};
assign data[38] = {8'd00,8'd00,8'd00};
assign data[39] = {8'd00,8'd00,8'd00};
//6
assign data[40] = {8'd00,8'd00,8'd00};
assign data[41] = {8'd00,8'd00,8'd00};
assign data[42] = {8'd00,8'd00,8'd00};
assign data[43] = {8'd00,8'd00,8'd00};
assign data[44] = {8'd00,8'd00,8'd00};
assign data[45] = {8'd00,8'd00,8'd00};
assign data[46] = {8'd00,8'd00,8'd00};
assign data[47] = {8'd00,8'd00,8'd00};
//7
assign data[48] = {8'd00,8'd00,8'd00};
assign data[49] = {8'd00,8'd00,8'd00};
assign data[50] = {8'd00,8'd00,8'd00};
assign data[51] = {8'd126,8'd255,8'd00};
assign data[52] = {8'd126,8'd255,8'd00};
assign data[53] = {8'd00,8'd00,8'd00};
assign data[54] = {8'd00,8'd00,8'd00};
assign data[55] = {8'd00,8'd00,8'd00};
//8
assign data[56] = {8'd00,8'd00,8'd00};
assign data[57] = {8'd00,8'd00,8'd00};
assign data[58] = {8'd126,8'd255,8'd00};
assign data[59] = {8'd126,8'd255,8'd00};
assign data[60] = {8'd126,8'd255,8'd00};
assign data[61] = {8'd126,8'd255,8'd00};
assign data[62] = {8'd00,8'd00,8'd00};
assign data[63] = {8'd00,8'd00,8'd00};

//----------------------------------------------
assign data1[00] = {8'd00,8'd00,8'd00};
assign data1[01] = {8'd00,8'd00,8'd00};
assign data1[02] = {8'd00,8'd00,8'd00};
assign data1[03] = {8'd00,8'd00,8'd00};
assign data1[04] = {8'd00,8'd00,8'd00};
assign data1[05] = {8'd00,8'd00,8'd00};
assign data1[06] = {8'd00,8'd00,8'd00};
assign data1[07] = {8'd00,8'd00,8'd00};
//21

assign data1[08] = {8'd00,8'd00,8'd00};
assign data1[09] = {8'd00,8'd00,8'd00};
assign data1[10] = {8'd00,8'd00,8'd00};
assign data1[11] = {8'd00,8'd00,8'd00};
assign data1[12] = {8'd00,8'd00,8'd00};
assign data1[13] = {8'd00,8'd00,8'd00};
assign data1[14] = {8'd00,8'd00,8'd00};
assign data1[15] = {8'd00,8'd00,8'd00};
//31
assign data1[16] = {8'd00,8'd00,8'd00};
assign data1[17] = {8'd00,8'd00,8'd00};
assign data1[18] = {8'd00,8'd00,8'd00};
assign data1[19] = {8'd00,8'd00,8'd00};
assign data1[20] = {8'd00,8'd00,8'd00};
assign data1[21] = {8'd00,8'd00,8'd00};
assign data1[22] = {8'd00,8'd00,8'd00};
assign data1[23] = {8'd00,8'd00,8'd00};
//41
assign data1[24] = {8'd00,8'd00,8'd00};
assign data1[25] = {8'd00,8'd00,8'd00};
assign data1[26] = {8'd00,8'd00,8'd00};
assign data1[27] = {8'd00,8'd00,8'd00};
assign data1[28] = {8'd00,8'd00,8'd00};
assign data1[29] = {8'd00,8'd00,8'd00};
assign data1[30] = {8'd00,8'd00,8'd00};
assign data1[31] = {8'd00,8'd00,8'd00};
//51
assign data1[32] = {8'd00,8'd00,8'd00};
assign data1[33] = {8'd00,8'd00,8'd00};
assign data1[34] = {8'd00,8'd00,8'd00};
assign data1[35] = {8'd00,8'd00,8'd00};
assign data1[36] = {8'd00,8'd00,8'd00};
assign data1[37] = {8'd00,8'd00,8'd00};
assign data1[38] = {8'd00,8'd00,8'd00};
assign data1[39] = {8'd00,8'd00,8'd00};
//61
assign data1[40] = {8'd00,8'd00,8'd00};
assign data1[41] = {8'd00,8'd00,8'd00};
assign data1[42] = {8'd00,8'd00,8'd00};
assign data1[43] = {8'd126,8'd255,8'd00};
assign data1[44] = {8'd126,8'd255,8'd00};
assign data1[45] = {8'd00,8'd00,8'd00};
assign data1[46] = {8'd00,8'd00,8'd00};
assign data1[47] = {8'd00,8'd00,8'd00};
//71
assign data1[48] = {8'd00,8'd00,8'd00};
assign data1[49] = {8'd00,8'd00,8'd00};
assign data1[50] = {8'd126,8'd255,8'd00};
assign data1[51] = {8'd126,8'd255,8'd00};
assign data1[52] = {8'd126,8'd255,8'd00};
assign data1[53] = {8'd126,8'd255,8'd00};
assign data1[54] = {8'd00,8'd00,8'd00};
assign data1[55] = {8'd00,8'd00,8'd00};
//81
assign data1[56] = {8'd00,8'd00,8'd00};
assign data1[61] = {8'd126,8'd255,8'd00};
assign data1[58] = {8'd126,8'd255,8'd00};
assign data1[59] = {8'd126,8'd255,8'd00};
assign data1[60] = {8'd126,8'd255,8'd00};
assign data1[61] = {8'd126,8'd255,8'd00};
assign data1[61] = {8'd126,8'd255,8'd00};
assign data1[63] = {8'd00,8'd00,8'd00};
//----------------------------------------------
assign data2[00] = {8'd00,8'd00,8'd00};
assign data2[01] = {8'd00,8'd00,8'd00};
assign data2[02] = {8'd00,8'd00,8'd00};
assign data2[03] = {8'd00,8'd00,8'd00};
assign data2[04] = {8'd00,8'd00,8'd00};
assign data2[05] = {8'd00,8'd00,8'd00};
assign data2[06] = {8'd00,8'd00,8'd00};
assign data2[07] = {8'd00,8'd00,8'd00};
//22

assign data2[08] = {8'd00,8'd00,8'd00};
assign data2[09] = {8'd00,8'd00,8'd00};
assign data2[10] = {8'd00,8'd00,8'd00};
assign data2[11] = {8'd00,8'd00,8'd00};
assign data2[12] = {8'd00,8'd00,8'd00};
assign data2[13] = {8'd00,8'd00,8'd00};
assign data2[14] = {8'd00,8'd00,8'd00};
assign data2[15] = {8'd00,8'd00,8'd00};
//3
assign data2[16] = {8'd00,8'd00,8'd00};
assign data2[17] = {8'd00,8'd00,8'd00};
assign data2[18] = {8'd00,8'd00,8'd00};
assign data2[19] = {8'd00,8'd00,8'd00};
assign data2[20] = {8'd00,8'd00,8'd00};
assign data2[21] = {8'd00,8'd00,8'd00};
assign data2[22] = {8'd00,8'd00,8'd00};
assign data2[23] = {8'd00,8'd00,8'd00};
//4
assign data2[24] = {8'd00,8'd00,8'd00};
assign data2[25] = {8'd00,8'd00,8'd00};
assign data2[26] = {8'd00,8'd00,8'd00};
assign data2[27] = {8'd00,8'd00,8'd00};
assign data2[28] = {8'd00,8'd00,8'd00};
assign data2[29] = {8'd00,8'd00,8'd00};
assign data2[30] = {8'd00,8'd00,8'd00};
assign data2[31] = {8'd00,8'd00,8'd00};
//5
assign data2[32] = {8'd00,8'd00,8'd00};
assign data2[33] = {8'd00,8'd00,8'd00};
assign data2[34] = {8'd00,8'd00,8'd00};
assign data2[35] = {8'd126,8'd255,8'd00};
assign data2[36] = {8'd126,8'd255,8'd00};
assign data2[37] = {8'd00,8'd00,8'd00};
assign data2[38] = {8'd00,8'd00,8'd00};
assign data2[39] = {8'd00,8'd00,8'd00};
//6
assign data2[40] = {8'd00,8'd00,8'd00};
assign data2[41] = {8'd00,8'd00,8'd00};
assign data2[42] = {8'd126,8'd255,8'd00};
assign data2[43] = {8'd126,8'd255,8'd00};
assign data2[44] = {8'd126,8'd255,8'd00};
assign data2[45] = {8'd126,8'd255,8'd00};
assign data2[46] = {8'd00,8'd00,8'd00};
assign data2[47] = {8'd00,8'd00,8'd00};
//7
assign data2[48] = {8'd00,8'd00,8'd00};
assign data2[49] = {8'd126,8'd255,8'd00};
assign data2[50] = {8'd126,8'd255,8'd00};
assign data2[51] = {8'd126,8'd255,8'd00};
assign data2[52] = {8'd126,8'd255,8'd00};
assign data2[53] = {8'd126,8'd255,8'd00};
assign data2[54] = {8'd126,8'd255,8'd00};
assign data2[55] = {8'd00,8'd00,8'd00};
//8
assign data2[56] = {8'd126,8'd255,8'd00};
assign data2[61] = {8'd126,8'd255,8'd00};
assign data2[58] = {8'd126,8'd255,8'd00};
assign data2[59] = {8'd126,8'd255,8'd00};
assign data2[60] = {8'd126,8'd255,8'd00};
assign data2[61] = {8'd126,8'd255,8'd00};
assign data2[61] = {8'd126,8'd255,8'd00};
assign data2[63] = {8'd126,8'd255,8'd00};

endmodule