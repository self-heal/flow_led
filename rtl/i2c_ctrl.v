module i2c_ctrl (
	input sys_clk,
	input sys_rst_n,
);

parameter I2C_DIV_FREQ = 5'd25;
reg [4:0] cnt_i2c;
wire add_cnt_i2c;
wire end_cnt_i2c;
reg i2c_clk;
always @(posedge sys_clk or negedge sys_rst_n)begin
    if(!sys_rst_n)begin
	cnt_i2c <= 5'd0;
    end
    else if begin
	if(add_cnt_i2c)begin
	cnt_i2c <= cnt_i2c + 5'd1;
	end
	else if(end_cnt_i2c)begin
	cnt_i2c <= 5'd0;
	end
    end
    else begin
	cnt_i2c <= 5'd0;
    end
end

assign add_cnt_i2c = 1'b1;
assign end_cnt_i2c = add_cnt_i2c && cnt_i2c == I2C_DIV_FREQ - 1;//add_cnt_i2c这是个开始的条件

always @(posedge sys_clk or negedge sys_rst_n)begin
    if(!sys_rst_n)begin
	i2c_clk = 1'b0
    end
    else if (end_cnt_i2c)begin
	i2c_clk = ~i2c_clk;
    end
    else begin
	i2c_clk = i2c_clk;
    end
end



endmodule