module ws2812_top (
	input 	wire sys_clk	,
	input 	wire sys_rst_n	,

	output	wire dout
);
wire 	[4:0] 	cnt_bit;
wire	[6:0] 	cnt_led;
wire 		bit;
wire    [2:0]	cnt_photo;
ws2812_ctrl inst_ws2812_ctrl
(
	.sys_clk		(sys_clk	),
	.sys_rst_n		(sys_rst_n	),
	.bit			(bit		),
	
	.dout			(dout		),
	.cnt_bit		(cnt_bit	),		//哪一bit
	.cnt_led		(cnt_led	),		//64个灯中哪个灯
	.cnt_photo		(cnt_photo	)		//哪一组照片
);

data_configure inst_data_configure
(
	.cnt_bit		(cnt_bit	),				//哪一bit
	.cnt_led		(cnt_led	),			//64个灯中哪个灯
	.cnt_photo		(cnt_photo	),			//哪一组照片

	.bit			(bit		)
	
);
endmodule